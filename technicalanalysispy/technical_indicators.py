import pandas as pd
import numpy as np


def relative_strength_index(ts_price: pd.Series, period: int):
    """Fetch the relative strength index of the time series in a series term.

    Parameters
    ==========
    :param ts_price: The time series of price.
    :type ts_price: pd.Series
    :param period: Lookback window.
    :type period: int

    Returns
    =======
    :return: The time series of the RSI.
    :rtype: pd.Series
    """
    price_diff = (ts_price - ts_price.shift(1)).dropna()
    up_price = pd.Series(0, index=price_diff.index)
    up_price[price_diff > 0] = price_diff[price_diff > 0]
    down_price = pd.Series(0, index=price_diff.index)
    down_price[price_diff < 0] = -price_diff[price_diff < 0]
    rsi = []
    for i in range(period, len(up_price)+1):
        up_mean = np.mean(up_price.values[(i - period): i], dtype=np.float32)
        up_down = np.mean(down_price.values[(i - period): i], dtype=np.float32)
        rsi.append(100*up_mean/(up_mean + up_down))
    rsi = pd.Series(rsi, index=price_diff.index[(period - 1):])

    return rsi


def simple_moving_average(ts_price: pd.Series, lookback_window: int, fmt='series'):
    """Calculate the simple moving average of a time series in either series or generator format.
    e.g. sma5_generator = simple_moving_average(Close, 5, 'generator')
            next(sma5_generator)

    Parameters
    ==========
    :param ts_price: The time series of price.
    :type ts_price: pd.Series
    :param lookback_window: Lookback window.
    :type lookback_window: int
    :param fmt: 'series' or 'generator'.
    :type fmt: str

    Returns
    =======
    :return: Moving average time series or an element.
    :rtype: list or generator
    """
    if fmt == 'series':
        sma = pd.Series([np.nan]*len(ts_price), index=ts_price.index)
        for i in range(lookback_window-1, len(ts_price)):
            sma[i] = np.mean(ts_price[(i-lookback_window+1):(i+1)])

        return sma

    elif fmt == 'generator':
        price_list = []
        n = len(ts_price)
        for i in range(n):
            price_list.append(ts_price[i])
            if len(price_list) == lookback_window:
                yield np.mean(price_list)
                price_list.pop(0)

    else:
        print(f'[ERROR] Incorrect input for the format(fmt).')
        return


def weighted_moving_average(ts_price: pd.Series, weight: list, fmt='series'):
    """Calculate the weighted moving average of a time series in either series or generator format.

    Parameters
    ==========
    :param ts_price: The time series of price.
    :type ts_price: pd.Series
    :param weight: The list of different weightings.
    :type weight: list
    :param fmt: 'series' or 'generator'.
    :type fmt: str

    Returns
    =======
    :return: Moving average time series or an element.
    :rtype: list or generator
    """
    if fmt == 'series':
        lookback_window = len(weight)
        arr_weight = np.array(weight)
        wma = pd.Series([np.nan]*len(ts_price), index=ts_price.index)
        for i in range(lookback_window-1, len(ts_price)):
            wma[i] = sum(arr_weight * ts_price[(i-lookback_window+1):(i+1)])

        return wma

    elif fmt == 'generator':
        price_list = []
        n = len(ts_price)
        w = np.array(weight)
        k = len(w)
        for i in range(n):
            price_list.append(ts_price[i])
            if len(price_list) == k:
                yield np.sum(np.array(price_list) * w)
                price_list.pop(0)

    else:
        print(f'[ERROR] Incorrect input for the format(fmt).')
        return


def exponentially_weighted_moving_average(ts_price: pd.Series, lookback_window: int, exponential=0.2, fmt='series'):
    """Calculate the exponentially weighted moving average of a time series in either series or generator format.

    Parameters
    ==========
    :param ts_price: The time series of price.
    :type ts_price: pd.Series
    :param lookback_window: Lookback window.
    :type lookback_window: int
    :param exponential: The exponential decay constant as a parameter.
    :type exponential: float
    :param fmt: 'series' or 'generator'.
    :type fmt: str

    Returns
    =======
    :return: Moving average time series or an element.
    :rtype: list or generator
    """
    if fmt == 'series':
        ewma = pd.Series([np.nan]*len(ts_price), index=ts_price.index)
        ewma[lookback_window-1] = np.mean(ts_price[:lookback_window])
        for i in range(lookback_window, len(ts_price)):
            ewma[i] = exponential*ts_price[i] + (1 - exponential)*ewma[i-1]

        return ewma

    elif fmt == 'generator':
        for i in range(lookback_window-1, len(ts_price)):
            if i == lookback_window - 1:
                ewma_value = np.mean(ts_price[:lookback_window])
            else:
                ewma_value = exponential * ts_price[i] + (1 - exponential)*ewma_value
            yield ewma_value

    else:
        print(f'[ERROR] Incorrect input for the format(fmt).')
        return
