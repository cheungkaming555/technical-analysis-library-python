# Specify whether you use pandas or numpy
import pandas as pd


class Candlestick:
    # def __repr__(self):
    #     return f'{self.df}'

    def __init__(self, df):
        self.df = df
        self.df.index = pd.to_datetime(self.df.Date, dayfirst=True)
        self._date = self.df.Date
        self._open = self.df.Open
        self._high = self.df.High
        self._low = self.df.Low
        self._close = self.df.Close
        self._volume = self.df.Volume

        self._range = self._high - self._low
        self._volatility = self._high / self._low - 1
        self._body_height = abs(self._open - self._close)
        self._shadow_height = self._range - self._body_height

        self._lower_shadow = []
        self._upper_shadow = []
        for i in range(self.df.__len__()):
            self._lower_shadow.append(min(self._open[i], self._close[i]) - self._low[i])
            self._upper_shadow.append(self._high[i] - max(self._open[i], self._close[i]))
        self._lower_shadow = pd.DataFrame(self._lower_shadow, index=self._close.index)
        self._upper_shadow = pd.DataFrame(self._upper_shadow, index=self._close.index)

    def median_range(self, no_of_bars):
        return self._range.rolling(window=no_of_bars, center=False).median()

    def median_shadow_height(self, no_of_bars):
        return self._shadow_height.rolling(window=no_of_bars, center=False).median()

    def median_body_height(self, no_of_bars):
        return self._body_height.rolling(window=no_of_bars, center=False).median()

    def median_volatility(self, no_of_bars):
        return self._volatility.rolling(window=no_of_bars, center=False).median()

    def average_volatility(self, no_of_bars):
        return self._volatility.rolling(window=no_of_bars, center=False).mean()

    def average_range(self, no_of_bars):
        return self._range.rolling(window=no_of_bars, center=False).mean()

    def average_body_height(self, no_of_bars):
        return self._body_height.rolling(window=no_of_bars, center=False).mean()

    def average_shadow_height(self, no_of_bars):
        return self._shadow_height.rolling(window=no_of_bars, center=False).mean()

    def is_small_bodied(self, i, no_of_bars=21):
        if self._body_height[i] <= self.median_body_height(no_of_bars=no_of_bars)[i]:
            return 1
        else:
            return 0

    def is_tall_candle(self, i, no_of_bars=21):
        if self._range[i] > self.median_range(no_of_bars=no_of_bars)[i]:
            return 1
        else:
            return 0

    def is_upward_price_trend(self, i, no_of_bars):
        for k in range(no_of_bars, 0, -1):
            if self._high[i - k] < self._high[i - k + 1] \
                    and self._low[i - k] < self._low[i - k + 1]:
                continue
            else:
                return 0
        return 1

    def is_downward_price_trend(self, i, no_of_bars):
        for k in range(no_of_bars, 0, -1):
            if self._high[i - k + 1] < self._high[i - k] \
                    and self._low[i - k + 1] < self._low[i - k]:
                continue
            else:
                return 0
        return 1

    def is_doji(self, i, no_of_bars=10, m=0.3):
        if abs(self._open - self._close) < self.average_range(no_of_bars=no_of_bars)[i] * m:
            return 1
        else:
            return 0

    def close_higher(self, i):
        if self._close[i] > self._close[i - 1]:
            return 1
        else:
            return 0

    def close_lower(self, i):
        if self._close[i] < self._close[i - 1]:
            return 1
        else:
            return 0

    def is_outside_bar(self, i):
        # today.high > ytd.high AND today.low < ytd.low
        if self._high[i] > self._high[i - 1] and self._low[i] < self._low[i - 1]:
            return 1
        else:
            return 0

    def is_inside_bar(self, i):
        if self._high[i - 1] > self._high[i] and self._low[i - 1] < self._low[i]:
            return 1
        else:
            return 0

    def is_engulfing_bar(self, i):
        # max(today.open, today.close) > ytd.high AND min(today.open, today.close) < ytd.low
        if max(self._open[i], self._close[i]) > self._high[i - 1] and min(self._open[i], self._close[i]) < self._low[i - 1]:
            return 1
        else:
            return 0

    def is_white(self, i):
        if self._close[i] > self._open[i]:
            return 1
        else:
            return 0

    def is_black(self, i):
        if self._close[i] < self._open[i]:
            return 1
        else:
            return 0

    def is_gap_up(self, i):
        # tmr.low > today.high
        if self._low[i] > self._high[i - 1]:
            return 1
        else:
            return 0

    def is_gap_down(self, i):
        # tmr.high < today.low
        if self._high[i] < self._low[i - 1]:
            return 1
        else:
            return 0

    def is_gap_up_open(self, i):
        # tmr.open > today.high
        if self._open[i] > self._high[i - 1]:
            return 1
        else:
            return 0

    def is_gap_down_open(self, i):
        # tmr.open < today.low
        if self._open[i] < self._low[i - 1]:
            return 1
        else:
            return 0

    def is_belt_hold_bearish(self, i, j=0.2, no_of_bars_tall=21, no_of_bars_upward_trend=3):
        # Candlestick Charts P.118
        # Price trend: Upward leading to the candle line.
        # Price opens at the high and closes near the low, creating a tall black candle.
        if self._open[i] == self._high[i] and (self._close[i] - self._low[i]) < j * self._range[i] \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall) and self.is_black(i=i) \
                and self.is_upward_price_trend(i=i, no_of_bars=no_of_bars_upward_trend):
            return 1
        else:
            return 0

    def is_belt_hold_bullish(self, i, j=0.2, no_of_bars_tall=21, no_of_bars_downward_trend=3):
        # Candlestick Charts P.127
        # Price trend: Downward leading to the candlestick.
        # A tall white candle with price opening at the low and closing near the high.
        if self._open[i] == self._low[i] and (self._high[i] - self._close[i]) < j * self._range[i] \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall) and self.is_white(i=i) \
                and self.is_downward_price_trend(i=i, no_of_bars=no_of_bars_downward_trend):
            return 1
        else:
            return 0

    def close_above_high(self, i, no_of_bars=1):
        if self._close[i] > max(self._high[i - no_of_bars:i]):
            return 1
        else:
            return 0

    def close_below_low(self, i, no_of_bars=1):
        if self._close[i] < min(self._low[i - no_of_bars:i]):
            return 1
        else:
            return 0

    def is_candle_black(self, i):    # Candlestick Charts P.146
        # A normal-size candle with shadows that do not exceed the body height. Black.
        if self._close[i] < self._open[i] and self._shadow_height[i] < self._body_height[i]:
            return 1
        else:
            return 0

    def is_candle_short_black(self, i, no_of_bars=21):   # Candlestick Charts P.154
        # A short candle with upper and lower shadows each shorter than the body. Black.
        if self._close[i] < self._open[i] and self.is_tall_candle(i=i, no_of_bars=no_of_bars) == 0 \
                and (self._high[i] - self._open[i]) < self._body_height[i] \
                and (self._close[i] - self._low[i]) < self._body_height[i]:
            return 1
        else:
            return 0

    def is_candle_short_white(self, i, no_of_bars=21):     # Candlestick Charts P.162
        # A short candlestick with each shadow shorter than the body height. White.
        if self._close[i] > self._open[i] and self.is_tall_candle(i=i, no_of_bars=no_of_bars) == 0 \
                and (self._high[i] - self._close[i]) < self._body_height[i] \
                and (self._open[i] - self._low[i]) < self._body_height[i]:
            return 1
        else:
            return 0

    def is_candle_white(self, i):    # Candlestick Charts P.170
        # A candle of normal size, neither short nor tall, with each shadow shorter than the body. White.
        if self._open[i] < self._close[i] and (self._high[i] - self._close[i]) < self._body_height[i] \
                and (self._open[i] - self._low[i]) < self._body_height[i]:
            return 1
        else:
            return 0

    def is_doji_dragonfly(self, i, no_of_bars=10, m=0.3, j=0.2, k=0.4):     # Candlestick charts P.202
        # Price opens and closes at or near the high for the day while having a long lower shadow.
        if self.is_doji(i=i, no_of_bars=no_of_bars, m=m) and (self._high[i] - self._close[i]) < self._range[i] * j \
                and (self._close[i] - self._low[i]) > self._range[i] * k:
            return 1
        else:
            return 0

    def is_doji_gapping_down(self, i, no_of_bars=10, m=0.3):  # Candlestick charts P.211
        # Price gaps lower from the prior day
        # Price opens and closes at or near the same price for the day
        # but today's upper shadow remains below the prior day's lower shadow, leaving a price gap on the chart
        if self.is_doji(i=i, no_of_bars=no_of_bars, m=m) and self.is_gap_down(i):
            return 1
        else:
            return 0

    def is_doji_gapping_up(self, i, no_of_bars=10, m=0.3):  # Candlestick charts P.221
        # Price gaps up form the prior day
        # Price opens and closes at or near the same price for the day (a doji),
        # but today’s lower shadow remains above the prior day’s upper shadow, leaving a price gap on the chart.
        if self.is_doji(i=i, no_of_bars=no_of_bars, m=m) and self.is_gap_up(i):
            return 1
        else:
            return 0

    def is_doji_gravestone(self, i, no_of_bars=10, m=0.3, j=0.2, k=0.4):  # Candlestick charts P.230
        # Price opens and closes at or very near the daily low with a tall upper shadow.
        if self.is_doji(i=i, no_of_bars=no_of_bars, m=m) and (self._close[i] - self._low[i]) < self._range[i] * j \
                and (self._high[i] - self._close[i]) > self._range[i] * k:
            return 1
        else:
            return 0

    def is_doji_long_legged(self, i, no_of_bars=10, m=0.3, j=0.35, k=0.35):  # Candlestick charts P.239
        # The opening and closing prices should be the same or nearly so, with long upper and lower shadows.
        if self.is_doji(i=i, no_of_bars=no_of_bars, m=m) and self._lower_shadow[i] > self._range[i] * j \
                and self._upper_shadow[i] > self._range[i] * k:
            return 1
        else:
            return 0

    def is_doji_northern(self, i, no_of_bars_doji=10, m=0.3, no_of_bars_upward_trend=3):    # Candlestick charts P.248
        # Price trend: Upward leading to the doji.
        # Price opens and closes at or near the same price.
        if self.is_doji(i=i, no_of_bars=no_of_bars_doji, m=m) \
                and self.is_upward_price_trend(i, no_of_bars=no_of_bars_upward_trend):
            return 1
        else:
            return 0

    def is_doji_southern(self, i, no_of_bars_doji=10, m=0.3, no_of_bars_downward_trend=3):   # Candlestick charts P.257
        # Price trend: Downward leading to the doji.
        # The open and close are near in price to each other.
        if self.is_doji(i=i, no_of_bars=no_of_bars_doji, m=m) \
                and self.is_upward_price_trend(i, no_of_bars=no_of_bars_downward_trend):
            return 1
        else:
            return 0

    def is_hammer(self, i, no_of_bars_downward_trend, j=0.2, no_of_bars=21):   # Candlestick charts P.348
        # Price trend: Downward leading to the candlestick pattern
        # Has a lower shadow between two and three times the height of a small body and little or no upper shadow.
        # Body color is unimportant
        if self.is_downward_price_trend(i, no_of_bars=no_of_bars_downward_trend) \
                and self._upper_shadow[i] < j * self._range[i] \
                and 3 * self._body_height[i] > self._lower_shadow[i] > 2 * self._body_height[i] \
                and self.is_small_bodied(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_hanging_man(self, i, no_of_bars_upward_trend, no_of_bars=21, j=0.2, k=0.4):  # Candlestick charts P.365
        # Price trend: Upward leading to the start of the hanging mam.
        # First day: Price opens at or near the high, forms a long lower shadow, and closes near, but not at, the high.
        # A small body remains, either white or black, near the top of the trading range with a long lower shadow.
        if self.is_upward_price_trend(i=i, no_of_bars=no_of_bars_upward_trend) and self._high[i] != self._close[i] \
                and (self._high[i] - self._open[i]) <= j * self._range[i] \
                and (self._high[i] - self._close[i]) <= j * self._range[i] \
                and self._lower_shadow[i] > k * self._range[i] \
                and self.is_small_bodied(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_high_wave(self, i, no_of_bars=21, k=0.4):     # Candlestick charts P.409
        # Tall upper and lower shadows with a small body. Body can be either black or white.
        if self._upper_shadow[i] > k * self._range[i] and self._lower_shadow[i] > k * self._range[i] \
                and self.is_small_bodied(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_long_black_day(self, i, no_of_bars=10):   # Candlestick charts P.488
        # The candle is black and the body height is at least three times the average body height of recent candles,
        # with shadows shorter than the body.
        if self.is_black(i) and self._body_height[i] >= 3 * self.average_body_height(no_of_bars=no_of_bars)[i] \
                and self._body_height[i] > self._shadow_height[i]:
            return 1
        else:
            return 0

    def is_long_white_day(self, i, no_of_bars=10):  # Candlestick charts P.496
        # A tall white candle with a body three times the average of the prior week or two
        # and with shadows shorter than the body.
        if self.is_white(i) and self._body_height[i] >= 3 * self.average_body_height(no_of_bars=no_of_bars)[i] \
                and self._body_height[i] > self._shadow_height[i]:
            return 1
        else:
            return 0

    def is_marubozu_black(self, i, no_of_bars=21):  # Candlestick charts P.504
        # A tall black body with no shadows.
        if self._shadow_height[i] == 0 and self.is_black(i) and self.is_tall_candle(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_marubozu_closing_black(self, i, no_of_bars=21):  # Candlestick charts P.512
        # A tall black candle with an upper shadow but no lower shadow.
        if self.is_black(i) and self._lower_shadow[i] == 0and self._upper_shadow[i] > 0 \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_marubozu_closing_white(self, i, no_of_bars=21):  # Candlestick charts P.520
        # A tall white body with a close at the high and a lower shadow.
        if self.is_white(i) and self._upper_shadow[i] == 0 and self._lower_shadow[i] > 0 \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_marubozu_opening_black(self, i, no_of_bars=21):  # Candlestick charts P.528
        # A tall black candle with a lower shadow but no upper shadow.
        if self.is_black(i) and self._upper_shadow[i] == 0 and self._lower_shadow[i] > 0 \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_marubozu_opening_white(self, i, no_of_bars=21):  # Candlestick charts P.536
        # A tall white candle with an upper shadow but no lower one.
        if self.is_white(i) and self._lower_shadow[i] == 0 and self._upper_shadow[i] > 0 \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_marubozu_white(self, i, no_of_bars=21):  # Candlestick charts P.544
        # A tall white candle without shadows.
        if self.is_white(i) and self._shadow_height[i] == 0 and self.is_tall_candle(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_rickshaw_man(self, i, no_of_bars_doji=10, m=0.3, r=2, no_of_bars=21, j=0.2):  # Candlestick charts P.625
        # A doji candle (open and close are the same price or nearly so) with the body near the middle of the candle
        # and with exceptionally long shadows.
        if self.is_doji(i=i, no_of_bars=no_of_bars_doji, m=m) \
                and self._shadow_height[i] > r * self.average_shadow_height(no_of_bars=no_of_bars)[i] \
                and abs(self._upper_shadow[i] - self._lower_shadow[i]) < j * self._range[i]:
            return 1
        else:
            return 0

    def is_shooting_star_one_candle(self, i, no_of_bars_upward_trend, no_of_bars=21, j=0.2):  # Candlestick charts P.660
        # Price trend: Upward leading to the start of the candle pattern.
        # Look for a tall upper shadow at least twice the body height above a small body.
        # The body should be at or near the candle’s low, with no lower shadow (or a very small one).
        if self.is_upward_price_trend(i=i, no_of_bars=no_of_bars_upward_trend) \
                and self._upper_shadow[i] >= 2 * self._body_height[i] \
                and self.is_small_bodied(i=i, no_of_bars=no_of_bars) and self._lower_shadow[i] < j * self._range[i]:
            return 1
        else:
            return 0

    def is_spinning_top_black(self, i, no_of_bars=21):  # Candlestick charts P.694
        # A small black body with shadows longer than the body.
        if self._shadow_height[i] > self._body_height[i] and self.is_black(i) \
                and self.is_small_bodied(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_spinning_top_white(self, i, no_of_bars=21):  # Candlestick charts P.702
        # A small white body with shadows longer than the body.
        if self._shadow_height[i] > self._body_height[i] and self.is_white(i) \
                and self.is_small_bodied(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_takuri_line(self, i, j=0.2, no_of_bars_downward_trend=3, no_of_bars=21):  # Candlestick charts P.720
        # Price trend: Downward leading to the start of the candle pattern.
        # A small candle body with no upper shadow, or a very small one,
        # and a lower shadow at least three times the height of the body.
        if self.is_downward_price_trend(i=i, no_of_bars=no_of_bars_downward_trend) \
                and self._upper_shadow[i] < j * self._range[i] \
                and self._lower_shadow[i] >= self._body_height[i] \
                and self.is_small_bodied(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    # Two Bars Patterns

    def is_above_the_stomach(self, i, no_of_bars_downward_trend=3):  # Candlestick charts P.89
        # Price trend: Downward
        # First day: black candle
        # Second day: White candle opening and closing at or above the midpoint of the prior black candle’s body.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) \
                and self.is_black(i-1) and self.is_white(i) \
                and self._open[i] > (self._open[i - 1] + self._close[i - 1])/2 \
                and self._close[i] > (self._open[i - 1] + self._close[i - 1])/2:
            return 1
        else:
            return 0

    def is_below_the_stomach(self, i, no_of_bars_upward_trend=3, no_of_bars_tall=21):  # Candlestick charts P.108
        # Price trend: Upward leading to the start of the candlestick.
        # First day: A tall white day.
        # Second day: The candle opens below the middle of the white candle’s body and closes at or below the middle, too.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) and self.is_white(i-1) \
                and self.is_black(i) and self._open[i] < (self._open[i - 1] + self._close[i - 1])/2 \
                and self._close[i] <= (self._open[i - 1] + self._close[i - 1])/2 \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall):
            return 1
        else:
            return 0

    def is_dark_cloud_cover(self, i, no_of_bars_upward_trend=3, no_of_bars_tall=21):  # Candlestick charts P.182
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white candle.
        # Second day: A black candle with the open above the prior high
        # and a close below the midpoint of the prior day’s body.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) and self.is_white(i-1) \
                and self.is_gap_up_open(i) and self.is_black(i) \
                and self._close[i] < (self._open[i - 1] + self._close[i - 1])/2 \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall):
            return 1
        else:
            return 0

    def is_doji_star_bearish(self, i, no_of_bars_upward_trend=3,
                             no_of_bars_long_white=10, no_of_bars_1=5, no_of_bars_2=10, m=0.3):
        # Candlestick charts P.266
        # Price trend: Upward leading to the pattern.
        # First day: A long white candle.
        # Body gap: Price gaps higher, forming a body that is above the first day’s body.
        # Second day: A doji. The open and close are at or near the same price.
        # Doji shadows: Avoid excessively long shadows on the doji.
        # The sum of the doji shadows is less than the body height of the prior day.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) \
                and self.is_long_white_day(i=i-1, no_of_bars=no_of_bars_long_white) \
                and min(self._open[i], self._close[i]) > self._close[i - 1] \
                and self._shadow_height[i] < 2 * self.average_shadow_height(no_of_bars=no_of_bars_1)[i] \
                and self.is_doji(i=i, no_of_bars=no_of_bars_2, m=m) \
                and self._shadow_height[i] < self._body_height[i - 1]:
            return 1
        else:
            return 0

    def is_doji_star_bullish(self, i, no_of_bars_downward_trend=3, no_of_bars_long_black=10,
                             no_of_bars_1=5, no_of_bars_2=10, m=0.3):
        # Candlestick charts P.276
        # Price trend: Downward leading to the candle.
        # First day: A tall black candle.
        # Body gap: The closing price of the first day is above the body of the doji.
        # Second day: A doji. The opening and closing prices of the doji remain below the prior day’s close,
        # even though the shadows may overlap.
        # Doji shadows: Discard patterns with unusually long doji shadows.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) \
                and self.is_long_black_day(i=i-1, no_of_bars=no_of_bars_long_black) \
                and self._close[i - 1] > max(self._close[i], self._open[i]) \
                and self._shadow_height[i] < 2 * self.average_shadow_height(no_of_bars=no_of_bars_1)[i] \
                and self.is_doji(i=i, no_of_bars=no_of_bars_2, m=m) \
                and self._shadow_height[i] < self._body_height[i - 1]:
            return 1
        else:
            return 0

    def is_engulfing_bearish(self, i, no_of_bars_upward_trend=3):  # Candlestick charts P.308
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A white candle.
        # Second day: A black candle, the body of which overlaps the white candle’s body.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) \
                and self.is_white(i-1) and self.is_black(i) \
                and self._open[i] > self._close[i - 1] and self._close[i] < self._open[i - 1]:
            return 1
        else:
            return 0

    def is_engulfing_bullish(self, i, no_of_bars_downward_trend=3):  # Candlestick charts P.317
        # Price trend: Downward leading to the start of the candlestick pattern.
        # First day: A black candle.
        # Second day: A white candle opens below the prior body and closes above that body, too.
        # Price need not engulf the shadows.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) \
                and self.is_black(i=i-1) and self.is_white(i=i) \
                and self._open[i] < self._close[i - 1] and self._close[i] > self._open[i - 1]:
            return 1
        else:
            return 0

    def is_hammer_inverted(self, i, no_of_bars_downward_trend=3, no_of_bars_tall=21,
                           j=0.2, k=0.4, nob=21, no_of_bars=10, m=0.3):
        # Candlestick charts P.356
        # Price trend: Downward leading to the candle pattern.
        # First day: A tall black candle with a close near the low of the day.
        # Second day: A small-bodied candle with a tall upper shadow and little or no lower shadow.
        # Body cannot be a doji (otherwise it’s a gravestone doji). The open must be below the prior day’s close.
        # Candle color is unimportant.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall) and self.is_black(i=i-1) \
                and (self._close[i - 1] - self._low[i - 1]) < j * self._range[i - 1] \
                and self.is_small_bodied(i=i, no_of_bars=nob) \
                and self._upper_shadow[i] > k * self._range[i] \
                and self._lower_shadow[i] < j * self._range[i] and self.is_doji(i=i, no_of_bars=no_of_bars, m=m) == 0 \
                and self._open[i] < self._close[i - 1]:
            return 1
        else:
            return 0

    def is_harami_bearish(self, i, no_of_bars_upward_trend=3, no_of_bars_tall=21):  # Candlestick charts P.374
        # Price trend: Upward leading to the candle pattern.
        # First day: A tall white candle. Some ignore the candle color, but I don’t.
        # Second day: A small black candle. The open and close must be within the body of the first day,
        # but ignore the shadows. Either the tops or the bottoms of the bodies can be equal but not both.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall) and self.is_white(i=i-1) \
                and self._open[i] <= self._close[i - 1] and self._close[i] >= self._open[i - 1] \
                and self.is_black(i=i) and self.is_small_bodied(i=i, no_of_bars=no_of_bars_tall) \
                and (self._open[i] == self._close[i - 1] and self._close[i] == self._open[i - 1]) == 0:
            return 1
        else:
            return 0

    def is_harami_bullish(self, i, no_of_bars_downward_trend=3, no_of_bars_tall=21):  # Candlestick charts P.383
        # Price trend: Downward leading to the candle pattern.
        # First day: A tall black candle.
        # Second day: A small-bodied white candle. The body must be within the prior candle’s body.
        # The tops or bottoms of the two bodies can be the same price but not both.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall) \
                and self.is_black(i=i-1) and self.is_white(i=i)  \
                and self._open[i] >= self._close[i - 1] and self._close[i] <= self._open[i - 1] \
                and (self._open[i] == self._close[i - 1] and self._close[i] == self._open[i - 1]) == 0 \
                and self.is_small_bodied(i=i, no_of_bars=no_of_bars_tall):
            return 1
        else:
            return 0

    def is_harami_cross_bearish(self, i, no_of_bars_upward_trend=3, no_of_bars_tall=21, no_of_bars=10, m=0.3):
        # Candlestick charts P.392
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white candle.
        # Second day: A doji (open and close are equal or nearly so)
        # with a trading range inside the price range of the prior day.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall) and self.is_white(i=i-1) \
                and self.is_doji(i=i, no_of_bars=no_of_bars, m=m) and self.is_inside_bar(i=i):
            return 1
        else:
            return 0

    def is_harami_cross_bullish(self, i, no_of_bars_downward_trend=3, no_of_bars_tall=21, no_of_bars=10, m=0.3):
        # Candlestick charts P.400
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall black candle.
        # Second day: A doji (open and closing prices are the same or nearly so)
        # with a high-low price range that fits inside the range of the black candle.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall) and self.is_black(i=i-1) \
                and self.is_doji(i=i, no_of_bars=no_of_bars, m=m) and self.is_inside_bar(i=i):
            return 1
        else:
            return 0

    def is_homing_pigeon(self, i, no_of_bars_downward_trend=3, no_of_bars_tall=21, no_of_bars=21):
        # Candlestick charts P.418
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall black body.
        # Second day: A short black body that is inside the body of the first day.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall) and self.is_black(i=i-1) \
                and self.is_black(i=i) and self.is_small_bodied(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_in_neck(self, i, no_of_bars_downward_trend=3, no_of_bars=10, j=0.2):  # Candlestick charts P.436
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A long black candle.
        # Second day: A white candle with an open below the low of the first day
        # and a close that is into the body of the first day but not by much.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) \
                and self.is_long_black_day(i=i-1, no_of_bars=no_of_bars) and self.is_gap_down_open(i=i) \
                and self._close[i - 1] < self._close[i] < self._open[i - 1] \
                and (self._close[i] - self._close[i - 1]) < j * self._range[i]:
            return 1
        else:
            return 0

    def is_kicking_bearish(self, i, no_of_bars=21):  # Candlestick charts P.444
        # First day: A white marubozu candle, meaning a tall white candle with no shadows.
        # Second day: A black marubozu candle, meaning a tall black candle with no shadows.
        # Price should gap below the white candle’s low.
        if self.is_marubozu_black(i=i, no_of_bars=no_of_bars) \
                and self.is_marubozu_white(i=i-1, no_of_bars=no_of_bars) and self.is_gap_down(i=i):
            return 1
        else:
            return 0

    def is_kicking_bullish(self, i, no_of_bars=21):  # Candlestick charts P.452
        # First day: A black marubozu candle: a tall black candle with no shadows.
        # Second day: Price gaps higher and a white marubozu candle forms: a tall white candle with no shadows.
        if self.is_marubozu_black(i=i-1, no_of_bars=no_of_bars) \
                and self.is_marubozu_white(i=i, no_of_bars=no_of_bars) and self.is_gap_up(i=i):
            return 1
        else:
            return 0

    def is_last_engulfing_bottom(self, i, no_of_bars_downward_trend=3):  # Candlestick charts P.470
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A white candle.
        # Second day: A black candle opens above the prior body and closes below the prior body.
        # Price need not engulf the shadows.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) and self.is_white(i=i-1) \
                and self.is_black(i=i) and self._open[i] > self._close[i - 1] and self._close[i] < self._open[i - 1]:
            return 1
        else:
            return 0

    def is_last_engulfing_top(self, i, no_of_bars_upward_trend=3):  # Candlestick charts P.479
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A black candle.
        # Second day: A white candle, the body of which overlaps the prior black candle’s body.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) and self.is_black(i=i-1) \
                and self.is_white(i=i) and self._close[i] > self._open[i - 1] and self._open[i] < self._close[i - 1]:
            return 1
        else:
            return 0

    def is_matching_low(self, i, no_of_bars_downward_trend=3, no_of_bars=10):  # Candlestick charts P.561
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall-bodied black candle.
        # Second day: A black body with a close that matches the prior close.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-1) \
                and self.is_black(i=i) and self._close[i] == self._close[i - 1] \
                and self._body_height[i - 1] > self.average_body_height(no_of_bars=no_of_bars)[i - 1]:
            return 1
        else:
            return 0

    def is_meeting_lines_bearish(self, i, no_of_bars_upward_trend=3, no_of_bars=21, j=0.2):  # Candlestick charts P.570
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white candle.
        # Second day: A tall black candle that closes at or near the prior day’s close.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-1) \
                and self.is_black(i=i) and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars) \
                and abs(self._close[i] - self._close[i - 1]) < j * self._range[i]:
            return 1
        else:
            return 0

    def is_meeting_lines_bullish(self, i, no_of_bars_downward_trend=3, no_of_bars=21, j=0.2):
        # Candlestick charts P.579
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall black candle.
        # Second day: A tall white candle with a closing price at or near the prior day’s close.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-1) \
                and self.is_white(i=i) and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars) \
                and abs(self._close[i] - self._close[i - 1]) < j * self._range[i]:
            return 1
        else:
            return 0

    def is_on_neck(self, i, no_of_bars_downward_trend=3, no_of_bars=21):
        # Candlestick charts P.607
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall black candle.
        # Second day: Price gaps lower but forms a white candle with a close that matches the prior low.
        if self.is_downward_price_trend(i=i - 1, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-1) \
                and self.is_white(i=i) and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars) \
                and self._close[i] == self._low[i - 1] and self._close[i - 1] > self._open[i]:
            return 1
        else:
            return 0

    def is_piercing_pattern(self, i, no_of_bars_downward_trend=3):
        # Candlestick charts P.616
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A black candle. Some require it to have a tall body, but I don’t.
        # Second day: A white candle that opens below the prior candle’s low and closes in the black body, between the midpoint and the open.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-1) \
                and self.is_white(i=i) and self.is_gap_down_open(i=i) \
                and (self._open[i - 1] + self._close[i - 1])/2 < self._close[i] < self._open[i - 1]:
            return 1
        else:
            return 0

    def is_separating_lines_bearish(self, i, no_of_bars_downward_trend=3, no_of_bars=21, j=0.2):
        # Candlestick charts P.642
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall white candle.
        # Second day: A tall black candle that opens at or near the same price as the prior candle opened.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) and self.is_white(i=i-1) \
                and self.is_black(i=i) and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars) \
                and abs(self._open[i] - self._open[i - 1]) < j * self._range[i]:
            return 1
        else:
            return 0

    def is_separating_lines_bullish(self, i, no_of_bars_upward_trend=3, no_of_bars=21, j=0.2):
        # Candlestick charts P.651
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall black candle.
        # Second day: A tall white candle that opens at or near the prior day’s open.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) and self.is_black(i=i-1) \
                and self.is_white(i=i) and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars) \
                and abs(self._open[i] - self._open[i - 1]) < j * self._range[i]:
            return 1
        else:
            return 0

    def is_shooting_star_two_candle(self, i, no_of_bars_upward_trend=3, j=0.2, no_of_bars=21):
        # Candlestick charts P.668
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A white candle.
        # Second day: A candle with an upper shadow at least three times the height of the body.
        # The small body is at the bottom end of the candle and the candle has no lower shadow or a very small one.
        # The body gaps above the prior day’s body.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-1) \
                and self._upper_shadow[i] >= 3 * self._body_height[i] \
                and min(self._open[i], self._close[i]) > self._close[i - 1] \
                and self._lower_shadow[i] < j * self._range[i] and self.is_small_bodied(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_thrusting(self, i, no_of_bars_downward_trend=3, j=0.2):
        # Candlestick charts P.803
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A black candle.
        # Second day: A white candle opens below the prior low and closes near but below the midpoint of the prior body.
        m = (self._open[i - 1] + self._close[i - 1]) / 2
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-1) \
                and self.is_white(i=i) and self.is_gap_down_open(i=i) and self._close[i] < m \
                and (m - self._close[i]) < self._range[i] * j:
            return 1
        else:
            return 0

    def is_tweezers_bottom(self, i, no_of_bars_downward_trend=3):  # Candlestick charts P.829
        # Price trend: Downward leading to the start of the candle pattern.
        # Two candles (any color) that share the same low price.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) and self._low[i] == self._low[i - 1]:
            return 1
        else:
            return 0

    def is_tweezers_top(self, i, no_of_bars_upward_trend=3):  # Candlestick charts P.837
        # Price trend: Upward leading to the start of the candle pattern.
        # Two adjacent candle lines (any color) share the same high price.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) and self._high[i] == self._high[i - 1]:
            return 1
        else:
            return 0

    def is_two_black_gapping_candles(self, i, no_of_bars_downward_trend=3):  # Candlestick charts P.845
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: Price gaps lower from the prior day and forms a black candle.
        # Second day: A lower high forms on the second black candle.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) and self.is_gap_down(i=i-1) \
                and self.is_black(i=i-1) and self.is_black(i=i) and self._high[i] < self._high[i - 1]:
            return 1
        else:
            return 0

    def is_window_falling(self, i, no_of_bars_downward_trend=3):  # Candlestick charts P.898
        # Price trend: Downward leading to the start of the candle pattern.
        # The high today is below the low of the prior day, leaving a gap on the chart.
        if self.is_downward_price_trend(i=i-1, no_of_bars=no_of_bars_downward_trend) and self.is_gap_down(i=i):
            return 1
        else:
            return 0

    def is_window_rising(self, i, no_of_bars_upward_trend=3):  # Candlestick charts P.903
        # Price trend: Upward leading to the start of the candle pattern.
        # The low today is above the high of the prior day, leaving a gap on the chart.
        if self.is_upward_price_trend(i=i-1, no_of_bars=no_of_bars_upward_trend) and self.is_gap_up(i=i):
            return 1
        else:
            return 0

    # Three Bars Patterns

    def is_abandoned_baby_bearish(self, i, no_of_bars_upward_trend=3, no_of_bars=10, m=0.3):  # Candlestick charts P.70
        # Price trend: Upward leading to the first candle.
        # First day: A white candle, either short or tall.
        # Second day: A doji whose lower shadow gaps above the prior and following days’ highs (above their upper shadows).
        # Last day: A black candle, either short or tall, with the upper shadow remaining below the doji’s lower shadow.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-2) \
                and self.is_gap_up(i=i-1) and self.is_black(i=i) and self.is_gap_down(i=i) \
                and self.is_doji(i=i-1, no_of_bars=no_of_bars, m=m):
            return 1
        else:
            return 0

    def is_abandoned_baby_bullish(self, i, no_of_bars_downward_trend=3, no_of_bars=10, m=0.3):  # Candlestick charts P.80
        # Price trend: Downward leading to the start of the candlestick pattern.
        # First day: Black candle.
        # Second day: Doji that gaps below the shadows of the candle lines on either side.
        # Last day: A white candle whose shadow gaps above the doji.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and self.is_gap_down(i=i-1) and self.is_white(i=i) and self.is_gap_up(i=i) \
                and self.is_doji(i=i-1, no_of_bars=no_of_bars, m=m):
            return 1
        else:
            return 0

    def is_advance_block(self, i, no_of_bars_upward_trend=3):  # Candlestick charts P.98
        # Price trend: Upward leading to the start of the candlestick pattern.
        # White for all three candles.
        # Price must open within the previous body.
        # Body: Tends to get shorter from the first candle to the last candle, but this is an observation, not a requirement.
        # Shadows: Taller on days 2 and 3.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) \
                and self.is_white(i=i-2) and self.is_white(i=i-1) and self.is_white(i=i) \
                and self._open[i - 2] < self._open[i - 1] < self._close[i - 2] \
                and self._open[i - 1] < self._open[i] < self._close[i - 1] \
                and self._shadow_height[i] > self._shadow_height[i - 2] \
                and self._shadow_height[i - 1] > self._shadow_height[i - 2]:
            return 1
        else:
            return 0

    def is_deliberation(self, i, no_of_bars_upward_trend=3, no_of_bars=21, r=1.5, j=0.2):  # Candlestick charts P.191
        # Price trend: Upward leading to the start of the candle pattern.
        # First and second days: Two long-bodied white candles.
        # Third day: A small body that opens near the second day’s close.
        # Open and close: Each candle opens and closes higher than the previous ones’ opens and closes.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-2) \
                and self.is_white(i=i-1) \
                and self._body_height[i - 2] > r * self.average_body_height(no_of_bars=no_of_bars)[i - 2] \
                and self._body_height[i - 1] > r * self.average_body_height(no_of_bars=no_of_bars)[i - 1] \
                and self.is_small_bodied(i=i, no_of_bars=no_of_bars) \
                and abs(self._open[i] - self._close[i - 1]) < j * self._range[i] \
                and self._close[i - 2] < self._close[i - 1] < self._close[i] \
                and self._open[i - 2] < self._open[i - 1] < self._open[i]:
            return 1
        else:
            return 0

    def is_doji_star_collapsing(self, i, no_of_bars_upward_trend=3, no_of_bars=10, m=0.3):  # Candlestick charts P.285
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A white candle.
        # Second day: A doji that gaps below yesterday’s candle, including the shadows.
        # Last day: A black day that gaps below the doji, including the shadows.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-2) \
                and self.is_gap_down(i=i-1) and self.is_black(i=i) and self.is_gap_down(i=i) \
                and self.is_doji(i=i, no_of_bars=no_of_bars, m=m):
            return 1
        else:
            return 0

    def is_downside_gap_three_methods(self, i, no_of_bars_downward_trend=3, r=1.5, no_of_bars=21):
        # Candlestick charts P.289
        # Price trend: Downward leading to the candle pattern.
        # First day: A long black-bodied candle.
        # Second day: Another long black-bodied candle with a gap between today and yesterday, including the shadows (meaning no overlapping shadows).
        # Third day: Price forms a white candle.
        # The candle opens within the body of the second day and closes within the body of the first candle, thus closing the gap between the two black candles.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) \
                and self.is_black(i=i-2) and self.is_black(i=i-1) and self.is_white(i=i) \
                and self._body_height[i - 2] > r * self.average_body_height(no_of_bars=no_of_bars)[i - 2] \
                and self._body_height[i - 1] > r * self.average_body_height(no_of_bars=no_of_bars)[i - 1] \
                and self._close[i - 1] <= self._open[i] <= self._open[i - 1] \
                and self._close[i - 2] <= self._close[i] <= self._open[i - 2]:
            return 1
        else:
            return 0

    def is_downside_tasuki_gap(self, i, no_of_bars_downward_trend=3):  # Candlestick charts P.299
        # Price trend: Downward leading to the start of the candlestick pattern.
        # First day: A black candle.
        # Second day: Price forms another black candle with a down gap between the first and second days. There should be no overlapping shadows.
        # Last day: Price forms a white candle that opens within the body of the second candle and closes within the gap set by the first and second day,
        # but does not completely close the gap. Ignore the shadows.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and self.is_gap_down(i=i-1) and self.is_black(i=i-1) and self.is_white(i=i) \
                and self._close[i - 1] < self._open[i] < self._open[i - 1] and self._high[i - 1] < self._close[i] < self._low[i - 2]:
            return 1
        else:
            return 0

    def is_evening_doji_star(self, i, no_of_bars_upward_trend=3, no_of_bars_tall=21, no_of_bars=10, m=0.3):
        # Candlestick charts P.326
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white day.
        # Second day: A doji that gaps above the bodies of the two adjacent candle lines.
        # The shadows are not important; only the doji body need remain above the surrounding candles.
        # Last day: A tall black candle that closes at or below the midpoint (well into the body) of the white candle.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) \
                and self.is_white(i=i-2) and self.is_black(i=i) and self.is_doji(i=i-1, no_of_bars=no_of_bars, m=m) \
                and min(self._close[i - 1], self._open[i - 1]) > self._close[i - 2] \
                and min(self._close[i - 1], self._open[i - 1]) > self._open[i] \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars_tall) \
                and self._close[i] <= (self._high[i - 2] + self._low[i - 2])/2 \
                and self._open[i - 2] < self._close[i] < self._close[i - 2]:
            return 1
        else:
            return 0

    def is_evening_star(self, i, no_of_bars_upward_trend=3, no_of_bars_tall=21, no_of_bars_bodied=21):
        # Candlestick charts P.335
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white candle.
        # Second day: A small-bodied candle that gaps above the bodies of the adjacent candles. It can be either black or white.
        # Last day: A tall black candle that gaps below the prior candle and closes at least halfway down the body of the white candle.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) \
                and self.is_black(i=i) and self.is_white(i=i-2) \
                and self._low[i - 1] > self._close[i - 2] and self._low[i - 1] > self._open[i] \
                and self._close[i] < (self._open[i - 2] + self._close[i - 2])/2 \
                and self.is_small_bodied(i=i-1, no_of_bars=no_of_bars_bodied) \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall):
            return 1
        else:
            return 0

    def is_identical_three_crows(self, i, no_of_bars_upward_trend=3, no_of_bars_tall=21, j=0.2):
        # Candlestick charts P.427
        # Price trend: Upward leading to the start of the pattern.
        # Look for three tall black candles, the last two each opening at or near the prior close.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) \
                and self.is_black(i=i-2) and self.is_black(i=i-1) and self.is_black(i=i) \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall) \
                and abs(self._open[i - 1] - self._close[i - 2]) < j * self._range[i - 1] \
                and abs(self._open[i] - self._close[i - 1]) < j * self._range[i]:
            return 1
        else:
            return 0

    def is_morning_doji_star(self, i, no_of_bars_downward_trend=3, no_of_bars_tall=21, no_of_bars=10, m=0.3):
        # Candlestick charts P.588
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall black candle.
        # Second day: A doji whose body gaps below the prior body.
        # Third day: A tall white candle whose body remains above the doji’s body.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and self.is_white(i=i) and self._high[i - 1] < self._close[i - 2] and self._high[i - 1] < self._open[i] \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall) \
                and self.is_doji(i=i-1, no_of_bars=no_of_bars, m=m):
            return 1
        else:
            return 0

    def is_morning_star(self, i, no_of_bars_downward_trend=3, no_of_bars_tall=21, no_of_bars=21):  # Candlestick charts P.598
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall black candle.
        # Second day: A small-bodied candle that gaps lower from the prior body. The color can be either black or white.
        # Third day: A tall white candle that gaps above the body of the second day and closes at least midway into the black body of the first day.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and max(self._open[i - 1], self._close[i - 1]) < self._close[i - 2] and self.is_white(i=i) \
                and max(self._open[i - 1], self._close[i - 1]) < self._open[i] \
                and self._close[i] >= (self._open[i - 2] + self._close[i - 2])/2 \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall) \
                and self.is_small_bodied(i=i-1, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_side_by_side_white_lines_bearish(self, i, no_of_bars_downward_trend=3, j=0.2):  # Candlestick charts P.676
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A black candle.
        # Days 2 and 3: White candles with bodies about the same size and opening prices near the same value.
        # The closing prices in both candles remain below the body of the black candle.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and self.is_white(i=i-1) and self.is_white(i=i) \
                and (self._open[i] - self._open[i - 1]) < j * self._range[i] \
                and max(self._close[i], self._close[i - 1]) < self._close[i - 2] \
                and abs(self._body_height[i - 1] - self._body_height[i]) < j * self._body_height[i]:
            return 1
        else:
            return 0

    def is_side_by_side_white_lines_bullish(self, i, no_of_bars_upward_trend=3, j=0.2):  # Candlestick charts P.685
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A white candle.
        # Days 2 and 3: Two white candles with bodies of similar size and opening prices near each other.
        # The bodies of both candles remain above the body of the first white candle, leaving a gap.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-2) \
                and self.is_white(i=i-1) and self.is_white(i=i) \
                and (self._open[i] - self._open[i - 1]) < j * self._range[i] \
                and min(self._open[i - 1], self._open[i]) > self._close[i - 2] \
                and abs(self._body_height[i - 1] - self._body_height[i]) < j * self._body_height[i]:
            return 1
        else:
            return 0

    def is_stick_sandwich(self, i, no_of_bars_downward_trend=3, j=0.2):  # Candlestick charts P.710
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A black candle.
        # Second day: A white candle that trades above the close of the first day.
        # Third day: A black candle that closes at or near the close of the first day.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and self.is_black(i=i) and self.is_white(i=i-1) and self._low[i - 1] > self._close[i - 2] \
                and (self._close[i] - self._close[i - 2]) < j * self._range[i]:
            return 1
        else:
            return 0

    def is_three_black_crows(self, i, no_of_bars_upward_trend=3, j=0.2, no_of_bars_tall=21):  # Candlestick charts P.728
        # Price trend: Upward leading to the start of the candle pattern.
        # Three tall black candles, each closing at a new low.
        # The last two candles open within the body of the previous candle. All should close at or near their lows.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_black(i=i-2) \
                and self.is_black(i=i-1) and self.is_black(i=i) and self._open[i - 2] > self._open[i - 1] > self._close[i - 2] \
                and self._open[i - 1] > self._open[i] > self._close[i - 1] \
                and self._close[i] < self._low[i - 1] and self._close[i - 1] < self._low[i - 2] \
                and (self._close[i - 2] - self._low[i - 2]) < j * self._range[i - 2] \
                and (self._close[i - 1] - self._low[i - 1]) < j * self._range[i - 1] \
                and (self._close[i] - self._low[i]) < j * self._range[i] \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall):
            return 1
        else:
            return 0

    def is_three_inside_down(self, i, no_of_bars_upward_trend=3, no_of_bars_tall=21, no_of_bars=21):
        # Candlestick charts P.738
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white candle.
        # Second day: A small black candle. The open and close must be within the body of the first day, but ignore the shadows.
        # Either the tops or the bottoms of the bodies can be equal, but not both.
        # Third day: Price must close lower.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_black(i=i-1) \
                and self.is_white(i=i-2) and self.is_black(i=i) \
                and self._close[i - 2] >= self._open[i - 1] and self._open[i - 2] <= self._close[i - 1] \
                and (self._open[i - 2] == self._close[i - 1] and self._close[i - 2] == self._open[i - 1]) == 0 \
                and self._close[i] < self._close[i - 1] and self.is_tall_candle(i=i - 2, no_of_bars=no_of_bars_tall) \
                and self.is_small_bodied(i=i-1, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_three_inside_up(self, i, no_of_bars_downward_trend=3, no_of_bars_tall=21, no_of_bars=21):
        # Candlestick charts P.747
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall black candle.
        # Second day: A small-bodied white candle. The body must be within the prior candle’s body.
        # The tops or bottoms of the two bodies can be the same price but not both.
        # Third day: A white candle that closes above the prior day’s close.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and self.is_white(i=i-1) and self.is_white(i=i) \
                and self._open[i - 2] >= self._close[i - 1] and self._close[i - 2] <= self._open[i - 1] \
                and (self._open[i - 2] == self._close[i - 1] and self._close[i - 2] == self._open[i - 1]) == 0 \
                and self._close[i] > self._close[i - 1] and self.is_tall_candle(i=i - 2, no_of_bars=no_of_bars_tall) \
                and self.is_small_bodied(i=i-1, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_three_outside_down(self, i, no_of_bars_upward_trend=3):  # Candlestick charts P.774
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A white candle.
        # Second day: A black candle opens higher and closes lower than the prior candle’s body, engulfing it.
        # Last day: A candle with a lower close.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-2) \
                and self.is_black(i=i-1) and self.is_black(i=i) and self._open[i - 1] > self._close[i - 2] \
                and self._close[i] < self._close[i - 1] < self._open[i - 2]:
            return 1
        else:
            return 0

    def is_three_outside_up(self, i, no_of_bars_downward_trend=3):  # Candlestick charts P.782
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A black candle.
        # Second day: A white candle opens below the prior body and closes above the body, too. Price need not engulf the shadows.
        # Last day: A white candle in which price closes higher.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and self.is_white(i=i-1) and self.is_white(i=i) and self._open[i - 1] < self._close[i - 2] \
                and self._close[i] > self._close[i - 1] > self._open[i - 2]:
            return 1
        else:
            return 0

    def is_three_stars_in_the_south(self, i, no_of_bars_downward_trend=3, k=0.4, no_of_bars=21):  # Candlestick charts P.790
        # Price trend: Downward leading to the start of the candle.
        # First day: A tall black candle with a long lower shadow.
        # Second day: Similar to the first day but smaller and with a low above the previous day’s low.
        # Last day: A black marubozu type candle fits inside the high-low trading range of the prior day.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and self.is_black(i=i-1) and self._lower_shadow[i - 2] > k * self._range[i - 2] \
                and self._low[i - 1] > self._low[i - 2] and self.is_outside_bar(i=i - 1) == 0 \
                and self.is_marubozu_black(i=i, no_of_bars=no_of_bars) and self.is_inside_bar(i=i) \
                and self._body_height[i - 2] > self._body_height[i - 1] and self._lower_shadow[i - 1] > k * self._range[i - 1] \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_three_white_soldiers(self, i, no_of_bars_downward_trend=3, j=0.2, no_of_bars_tall=21):
        # Candlestick charts P.794
        # Price trend: Downward leading to the start of the candle pattern.
        # Three days: Tall white candles with higher closes and price that opens within the previous body.
        # Price should close near the high each day.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_white(i=i-2) \
                and self.is_white(i=i-1) and self.is_white(i=i) \
                and self._open[i - 2] < self._open[i - 1] < self._close[i - 2] < self._close[i - 1] < self._close[i] \
                and self._open[i - 1] < self._open[i] < self._close[i - 1] and self._upper_shadow[i - 2] < j * self._range[i - 2] \
                and self._upper_shadow[i - 1] < j * self._range[i - 1] and self._upper_shadow[i] < j * self._range[i] \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars_tall) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall):
            return 1
        else:
            return 0

    def is_tri_star_bearish(self, i, no_of_bars_upward_trend=3, no_of_bars=10, m=0.3):  # Candlestick charts P.812
        # Price trend: Upward leading to the start of the candle pattern.
        # Look for three doji, the middle one has a body above the other two.
        a = min(self._open[i - 1], self._close[i - 1])
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) \
                and m > max(self._open[i - 2], self._close[i - 2]) and m > max(self._close[i], self._open[i]) \
                and self.is_doji(i=i-2, no_of_bars=no_of_bars, m=m) \
                and self.is_doji(i=i-1, no_of_bars=no_of_bars, m=m) \
                and self.is_doji(i=i, no_of_bars=no_of_bars, m=m):
            return 1
        else:
            return 0

    def is_tri_star_bullish(self, i, no_of_bars_downward_trend=3, no_of_bars=10, m=0.3):  # Candlestick charts P.820
        # Price trend: Downward leading to the start of the candle pattern.
        # Three doji. The middle one has a body below the other two.
        a = max(self._open[i - 1], self._close[i - 1])
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) \
                and a < min(self._open[i - 2], self._close[i - 2]) and a < min(self._open[i], self._close[i]) \
                and self.is_doji(i=i-2, no_of_bars=no_of_bars, m=m) \
                and self.is_doji(i=i-1, no_of_bars=no_of_bars, m=m) \
                and self.is_doji(i=i, no_of_bars=no_of_bars, m=m):
            return 1
        else:
            return 0

    def is_two_crows(self, i, no_of_bars_upward_trend=3, no_of_bars=21):  # Candlestick charts P.853
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white candle.
        # Second day: A black candle with a body that gaps above the prior body.
        # Third day: A black candle that opens within the prior body and closes within the white candle’s body (first day).
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-2) \
                and self.is_black(i=i-1) and self.is_black(i=i) \
                and self._open[i - 1] >= self._open[i] >= self._close[i - 1] > self._close[i - 2] >= self._close[i] >= self._open[i - 2] \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_unique_three_river_bottom(self, i, no_of_bars_downward_trend=3, no_of_bars=21):  # Candlestick charts P.861
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall-bodied black candle.
        # Second day: The black body is inside the prior body, but the low price (long lower shadow) is below the prior day’s low.
        # Third day: A short-bodied white candle, which is below the body of the prior day.
        if self.is_downward_price_trend(i=i-2, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-2) \
                and self.is_black(i=i-1) and self.is_white(i=i) and self._open[i - 2] > self._open[i - 1] \
                and self._close[i - 2] < self._close[i - 1] and self._low[i - 1] < self._low[i - 2] \
                and self._close[i] < self._close[i - 1] and self.is_small_bodied(i=i, no_of_bars=no_of_bars) \
                and self.is_small_bodied(i=i-2, no_of_bars=no_of_bars) == 0:
            return 1
        else:
            return 0

    def is_upside_gap_three_methods(self, i, no_of_bars_upward_trend=3, no_of_bars=21):  # Candlestick charts P.870
        # Price trend: Upward leading to the start of the candle pattern.
        # Days 1 and 2: Two tall white candles with a gap between them, even between the shadows.
        # Last day: A black candle fills the gap.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-2) \
                and self.is_white(i=i-1) and self.is_black(i=i) and self.is_gap_up(i=i-1) \
                and self._high[i] >= self._low[i - 1] and self._low[i] <= self._high[i - 2] \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_upside_gap_two_crows(self, i, no_of_bars_upward_trend=3, no_of_bars=21):  # Candlestick charts P.879
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white candle.
        # Second day: A black candle with a body gapping above the prior candle’s body.
        # Third day: A black candle that engulfs the body of the prior day. The close remains above the close of the first day.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_white(i-2) \
                and self.is_black(i=i-1) and self.is_black(i=i) and self._close[i - 1] > self._close[i - 2] \
                and self._close[i - 1] >= self._close[i] > self._close[i - 2] and self._open[i] >= self._open[i - 1] \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_upside_tasuki_gap(self, i, no_of_bars_upward_trend=3):  # Candlestick charts P.888
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A white candle.
        # Second day: A white candle. Price gaps higher, including the shadows, leaving a rising window between the two candles.
        # Third day: A black candle opens in the body of the prior candle and closes within the gap. The gap remains open if you ignore the lower shadow.
        if self.is_upward_price_trend(i=i-2, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-2) \
                and self.is_white(i=i-1) and self.is_black(i=i) and self.is_gap_up(i=i-1) \
                and self._open[i - 1] < self._open[i] < self._close[i - 1] and self._high[i - 2] < self._close[i] < self._low[i - 1]:
            return 1
        else:
            return 0

    # Four Bars Patterns

    def is_concealing_baby_swallow(self, i, no_of_bars_downward_trend=3, no_of_bars=21, k=0.4):
        # Candlestick charts P.178
        # Price trend: Downward leading to the start of the candle pattern.
        # First and second day: Two long black candles without any shadows (both are black marubozu candles).
        # Third day: A black day with a tall upper shadow. The candle gaps open downward and yet trades into the body of the prior day.
        # Last day: Another black day that completely engulfs the prior day, including the shadows.
        if self.is_downward_price_trend(i=i-3, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-1) \
                and self.is_marubozu_black(i=i-3, no_of_bars=no_of_bars) and self.is_black(i=i) \
                and self.is_marubozu_black(i=i-2, no_of_bars=no_of_bars) \
                and self._upper_shadow[i - 1] > k * self._range[i - 1] and self.is_gap_down_open(i=i - 1) \
                and self.is_engulfing_bar(i=i) and self._high[i - 1] > self._low[i - 2]:
            return 1
        else:
            return 0

    def is_three_line_strike_bearish(self, i, no_of_bars_downward_trend=3):  # Candlestick charts P.756
        # Price trend: Downward leading to the start of the candle pattern.
        # Days 1 to 3: Three black candles form lower closes.
        # Last day: A white candle opens below the prior close and closes above the first day’s open.
        if self.is_downward_price_trend(i=i-3, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-3) \
                and self.is_black(i=i-2) and self.is_black(i=i-1) and self.is_white(i=i) \
                and self._close[i - 3] > self._close[i - 2] > self._close[i - 1] > self._open[i] \
                and self._close[i] > self._open[i - 3]:
            return 1
        else:
            return 0

    def is_three_line_strike_bullish(self, i, no_of_bars_upward_trend=3):  # Candlestick charts P.765
        # Price trend: Upward leading to the start of the candle pattern.
        # Days 1 to 3: Three white candles, each with a higher close.
        # Last day: A black candle that opens higher but closes below the open of the first candle.
        if self.is_upward_price_trend(i=i-3, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-3) \
                and self.is_white(i=i-2) and self.is_white(i=i-1) and self.is_black(i=i) \
                and self._open[i] > self._close[i - 1] > self._close[i - 2] > self._close[i - 3] \
                and self._close[i] < self._open[i - 3]:
            return 1
        else:
            return 0

    # Multiple Bars Patterns

    def is_8_new_price_lines(self, i):  # Candlestick charts P.27
        # Each day should make a higher high, but the candles can be black or white.
        j = 0
        for k in reversed(range(8)):
            if self._high[i - k - 1] < self._high[i - k]:
                j = j+1
        if j == 8:
            return 1
        else:
            return 0

    def is_10_new_price_lines(self, i):  # Candlestick charts P.38
        # Ten consecutively higher highs
        j = 0
        for k in reversed(range(10)):
            if self._high[i - k - 1] < self._high[i - k]:
                j = j+1
        if j == 10:
            return 1
        else:
            return 0

    def is_12_new_price_lines(self, i):  # Candlestick charts P.49
        # Twelve consecutively higher highs.
        j = 0
        for k in reversed(range(12)):
            if self._high[i - k - 1] < self._high[i - k]:
                j = j+1
        if j == 12:
            return 1
        else:
            return 0

    def is_13_new_price_lines(self, i):  # Candlestick charts P.60
        # Thirteen consecutively higher highs.
        j = 0
        for k in reversed(range(13)):
            if self._high[i - k - 1] < self._high[i - k]:
                j = j+1
        if j == 13:
            return 1
        else:
            return 0

    def is_breakaway_bearish(self, i, no_of_bars_upward_trend=3, no_of_bars=21):  # Candlestick charts P.137
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white candle.
        # Second day: A white candle that has a gap between the two candle bodies, but the shadows can overlap.
        # Third day: A candle with a higher close, but the candle can be any color.
        # Fourth day: A white candle with a higher close.
        # Last day: A tall black candle with a close within the gap between the first two candle bodies.
        # Ignore the shadows on the first two candles for citing the gap.
        if self.is_upward_price_trend(i=i-4, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-4) \
                and self.is_white(i=i-3) and self.is_white(i=i-1) and self.is_black(i=i) \
                and self._open[i - 3] > self._close[i - 4] and self._close[i - 4] < self._close[i] \
                and self._close[i - 1] > self._close[i - 2] > self._close[i - 3] \
                and self.is_tall_candle(i=i-4, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_breakaway_bullish(self, i, no_of_bars_downward_trend=3, no_of_bars=21):  # Candlestick charts P.141
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall black candle.
        # Second day: A black candle that has a gap between the two candle bodies, but the shadows can overlap.
        # Third day: A candle with a lower close, but the candle can be any color.
        # Fourth day: A black candle with a lower close.
        # Last day: A tall white candle with a close within the gap between the first two candles.
        # Ignore the shadows on the first two candles for citing the gap.
        if self.is_downward_price_trend(i=i-4, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-4) \
                and self.is_black(i=i-3) and self.is_black(i=i-1) and self.is_white(i=i) \
                and self._close[i - 4] > self._open[i - 3] and self._close[i - 1] < self._close[i - 2] < self._close[i - 3] \
                and self._close[i - 4] > self._close[i] and self.is_tall_candle(i=i - 4, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_falling_three_methods(self, i, no_of_bars_downward_trend=3, no_of_bars=21):  # Candlestick charts P.344
        # Price trend: Downward leading to the start of the candle pattern.
        # First day: A tall black candle.
        # Days 2 to 4: Short candles, the middle one of which can be either black or white but the others are white.
        # The three trend upward but remain within the high-low range of the first day.
        # Last day: A tall black candle with a close below the first day’s close.
        if self.is_downward_price_trend(i=i-4, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-4) \
                and self.is_white(i=i-3) and self.is_white(i=i-1) \
                and max(self._high[i - 1], self._high[i - 2], self._high[i - 3]) <= self._high[i - 4] \
                and min(self._low[i - 1], self._low[i - 2], self._low[i - 3]) >= self._low[i - 4] \
                and self._close[i] < self._close[i - 4] \
                and self.is_tall_candle(i=i-4, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i-3, no_of_bars=no_of_bars) == 0 \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars) == 0 \
                and self.is_tall_candle(i=i-1, no_of_bars=no_of_bars) == 0:
            return 1
        else:
            return 0

    def is_ladder_bottom(self, i, no_of_bars_downward_trend=3, no_of_bars=21):  # Candlestick charts P.459
        # Price trend: Downward leading to the start of the candle pattern.
        # Days 1 to 3: Tall black candles, each with a lower open and lower close.
        # Fourth day: A black candle with an upper shadow.
        # Last day: A white candle that gaps above the body of the prior day.
        if self.is_downward_price_trend(i=i-4, no_of_bars=no_of_bars_downward_trend) and self.is_black(i=i-4) \
                and self.is_black(i=i-3) and self.is_black(i=i-2) and self.is_black(i=i-1) and self.is_white(i=i) \
                and self._open[i - 4] > self._open[i - 3] > self._open[i - 2] \
                and self._close[i - 4] > self._close[i - 3] > self._close[i - 2] and self._upper_shadow[i - 1] > 0 \
                and self._open[i] > self._open[i - 1] and self.is_tall_candle(i=i - 4, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i-3, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i-2, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_mat_hold(self, i, no_of_bars_upward_trend=3, no_of_bars_long=10, no_of_bars_tall=21, no_of_bars=21):
        # Candlestick charts P.552
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A long white body.
        # Second day: Price gaps open upward but closes lower (meaning a small black candle) and yet remains above the prior close. Ignore the shadows.
        # Days 3 and 4: Day 3 can be any color, but day 4 is black. Both candles have small bodies with the closing price easing lower,
        # but the bodies remain above the low of the first day.
        # Last day: A white candle with a close above the highs of the prior four candles.
        if self.is_upward_price_trend(i=i-4, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i) \
                and self.is_long_white_day(i=i-4, no_of_bars=no_of_bars_long) and self.is_black(i=i-3) \
                and self.is_tall_candle(i=i-3, no_of_bars=no_of_bars_tall) == 0 \
                and self._close[i - 3] > self._close[i - 4] and self.is_black(i=i - 1) \
                and self._close[i - 3] > self._close[i - 2] > self._close[i - 1] > self._low[i - 4] \
                and self._close[i] > max(self._high[i - 4], self._high[i - 3], self._high[i - 2], self._high[i - 1]) \
                and self.is_small_bodied(i=i-2, no_of_bars=no_of_bars) \
                and self.is_small_bodied(i=i-1, no_of_bars=no_of_bars):
            return 1
        else:
            return 0

    def is_rising_three_methods(self, i, no_of_bars_upward_trend=3, no_of_bars=21, no_of_bars_tall=21):
        # Candlestick charts P.633
        # Price trend: Upward leading to the start of the candle pattern.
        # First day: A tall white-bodied candle.
        # Days 2 to 4: Small-bodied candles that trend lower and close within the high-low range of the first candle.
        # Day 3 can be black or white, but days 2 and 4 are black candles.
        # Last day: A tall white candle that closes above the close of the first day.
        if self.is_upward_price_trend(i=i-4, no_of_bars=no_of_bars_upward_trend) and self.is_white(i=i-4) \
                and self.is_black(i=i-3) and self.is_black(i=i-1) and self.is_white(i=i) \
                and max(self._high[i - 1], self._high[i - 2], self._high[i - 3]) <= self._high[i - 4] \
                and min(self._low[i - 1], self._low[i - 2], self._low[i - 3]) >= self._low[i - 4] \
                and self.is_downward_price_trend(i=i-1, no_of_bars=3) \
                and self.is_small_bodied(i=i-4, no_of_bars=no_of_bars) == 0 \
                and self.is_small_bodied(i=i-3, no_of_bars=no_of_bars) \
                and self.is_small_bodied(i=i-2, no_of_bars=no_of_bars) \
                and self.is_small_bodied(i=i-1, no_of_bars=no_of_bars) \
                and self.is_tall_candle(i=i, no_of_bars=no_of_bars_tall) and self._close[i] > self._close[i - 4]:
            return 1
        else:
            return 0

